# -*- coding: utf-8 -*-
"""
Created on Thu Sep 19 21:31:44 2019

@author: Salma Ibrahim
"""

import sys
import requests

#Given matrix that is used in the Matrix Encryption
encryptMatrix = [[8.000, 4.000, 4.000, 8.000, 7.000, 8.000, 7.000, 1.000, 9.000, 4.000, 1.000, 1.000, 1.000, 6.000, 3.000, 0.000],
                [9.000, 6.000, 0.000, 0.000, 5.000, 4.000, 2.000, 3.000, 1.000, 4.000, 7.000, 8.000, 2.000, 6.000, 0.000, 5.000],
                [0.000, 7.000, 2.000, 4.000, 4.000, 9.000, 4.000, 0.000, 1.000, 1.000, 3.000, 1.000, 2.000, 7.000, 0.000, 7.000],
                [9.000, 5.000, 9.000, 7.000, 8.000, 3.000, 2.000, 6.000, 5.000, 5.000, 6.000, 1.000, 6.000, 8.000, 4.000, 6.000],
                [5.000, 1.000, 7.000, 1.000, 1.000, 0.000, 2.000, 9.000, 8.000, 6.000, 0.000, 9.000, 5.000, 5.000, 6.000, 3.000],
                [3.000, 9.000, 8.000, 4.000, 0.000, 4.000, 5.000, 0.000, 1.000, 0.000, 9.000, 6.000, 6.000, 7.000, 0.000, 7.000],
                [7.000, 1.000, 8.000, 3.000, 1.000, 0.000, 2.000, 1.000, 0.000, 1.000, 3.000, 3.000, 7.000, 5.000, 5.000, 3.000],
                [6.000, 1.000, 4.000, 3.000, 9.000, 5.000, 1.000, 2.000, 9.000, 7.000, 5.000, 4.000, 2.000, 5.000, 0.000, 0.000],
                [9.000, 3.000, 7.000, 5.000, 0.000, 4.000, 4.000, 2.000, 5.000, 9.000, 3.000, 1.000, 6.000, 6.000, 7.000, 4.000],
                [3.000, 2.000, 3.000, 4.000, 4.000, 1.000, 5.000, 1.000, 1.000, 0.000, 6.000, 5.000, 1.000, 2.000, 8.000, 3.000],
                [6.000, 6.000, 0.000, 7.000, 2.000, 7.000, 1.000, 0.000, 2.000, 6.000, 4.000, 2.000, 2.000, 9.000, 4.000, 0.000],
                [9.000, 7.000, 7.000, 3.000, 1.000, 5.000, 1.000, 0.000, 9.000, 5.000, 4.000, 9.000, 3.000, 6.000, 6.000, 0.000],
                [6.000, 4.000, 9.000, 3.000, 9.000, 7.000, 8.000, 8.000, 3.000, 8.000, 2.000, 1.000, 5.000, 9.000, 5.000, 2.000],
                [4.000, 2.000, 1.000, 7.000, 0.000, 2.000, 7.000, 9.000, 2.000, 8.000, 5.000, 7.000, 6.000, 7.000, 1.000, 9.000],
                [4.000, 2.000, 4.000, 2.000, 9.000, 4.000, 6.000, 2.000, 2.000, 6.000, 3.000, 0.000, 6.000, 7.000, 3.000, 2.000],
                [5.000, 9.000, 1.000, 4.000, 2.000, 3.000, 1.000, 0.000, 0.000, 8.000, 5.000, 6.000, 9.000, 9.000, 7.000, 0.000]]

#Given matrix that is used in the Matrix Decryption    
decryptMatrix = [[0.087698730443907, 0.106508423872910, -0.035837889522160, -0.018212614575464,
                     0.029282205872323, 0.009768535961213, 0.051022092452304, -0.002600159007516,
                     0.062599483798891, -0.020588660945658, -0.005731646667785, -0.109625191395912,
                     -0.006932407372092, -0.056202668885562, -0.075318484801680, 0.014708766520769],
                     [-0.022928256564172, -0.026683449299678, -0.011012588526442, 0.129942414512966,
                     -0.122921871300045, -0.043058741765130, -0.117420500612853, -0.140391873840202,
                     -0.071045583264847, -0.034148665820787, -0.095666764302039, 0.234091140082102,
                     0.027357920334586, 0.072726676711471, 0.065330793324812, 0.030966972897445],
                     [-0.218268560384352, -0.167198025185273, 0.018741969126614, 0.134524170075639,
                     -0.321465832541972, -0.166586732147428, 0.039385178742807, -0.113346935522171,
                     -0.185692852729154, -0.043259247663190, -0.076549003088936, 0.573616991323091,
                     0.104219031977534, 0.246544562428294, 0.177605218056100, -0.134494179895595],
                     [-0.074234710142859, -0.152484831242658, 0.032888774006722, 0.126780472517150,
                     -0.280349992738819, -0.177762951540922, 0.068628817353806, -0.051217841640809,
                     -0.180654642158176, -0.011766502223790, -0.080775821814310, 0.379125349840351,
                     0.048682798429393, 0.251118885929070, 0.025753297544241, -0.015741279938887],
                     [-0.107282379497940, -0.076473612678836, 0.048411952177752, 0.128387746395734,
                     -0.218476883182669, -0.182054566825948, 0.015297614620229, -0.071068523564674,
                     -0.173515463207588, -0.004127099161250, -0.106414518336578, 0.363864949305460,
                     0.024524217055173, 0.165306494873899, 0.163356977791568, -0.035033674480292],
                     [0.277288909540715, 0.191426836794975, 0.115975544752238, -0.300809424418599,
                     0.353820685363440, 0.206982195506894, 0.135654125071293, 0.381774054614345,
                     0.313562376027531, 0.069054897975148, -0.011797886745698, -0.853234848281320,
                     0.086056587100296, -0.364305268023283, -0.618093105304007, 0.253957528660826],
                     [0.038358445191113, -0.000546407470896, -0.073465777300804, -0.033200515562292,
                     -0.020621719941806, 0.046779969176516, -0.057290779719178, -0.095971425211909,
                     -0.009648470448360, 0.006680795638773, 0.001875819452453, 0.062297723542825,
                     -0.005182609330966, 0.040283770455199, 0.144418033011726, -0.055272938421632],
                     [0.241341634348029, 0.173795889482842, -0.037558951477139, -0.142555330724481,
                     0.384872718455622, 0.238727072083511, -0.012025720907547, 0.205726531015318,
                     0.215305804451694, 0.050086219117152, 0.058130045507594, -0.741794720895175,
                     0.050078370236749, -0.310037624023072, -0.430832649690117, 0.179950027829876],
                     [0.171381216925833, 0.086060780454874, -0.042565007877466, -0.078956143623505,
                     0.327447146261980, 0.206393272411101, -0.099952932487210, 0.087552045773402,
                     0.173129037727576, 0.018397910434386, 0.086685394445941, -0.429207905148962,
                     -0.134850123674970, -0.238397133799728, -0.059141482129532, 0.042322030383679],
                     [-0.288768790079429, -0.180939915747826, 0.014729697120086, 0.167827000966843,
                     -0.432864619931572, -0.272139265354751, -0.055798856798054, -0.158129080251415,
                     -0.179029372582232, -0.057409513342173, -0.107043502580043, 0.743098244369843,
                     0.075665395931411, 0.366662136199027, 0.301325496735494, -0.127959853885404],
                     [0.176761191401067, 0.163276217872526, -0.090326263481081, -0.179551087083123,
                     0.363256815341333, 0.329970109430809, -0.051879221066062, 0.256728747731346,
                     0.277043384431290, 0.104113728292426, 0.139981371677002, -0.742130479625641,
                     -0.034524073033884, -0.343286483781855, -0.306301246917888, 0.117604350114848],
                     [-0.145793544926800, -0.103188494699912, 0.058788410241278, 0.051655038640426,
                     -0.264038008105031, -0.203541647757149, 0.090418018220672, -0.064107759386820,
                     -0.219857426756124, -0.016087922861208, -0.104557302929551, 0.488954502811962,
                     0.057922132921064, 0.258682714654372, 0.111924825898909, -0.052708836311705],
                     [0.347740013692901, 0.186040292334817, 0.031705891858216, -0.245707324109423,
                     0.427598565702216, 0.270498331607873, 0.111400473365868, 0.327817668038880,
                     0.302461445968564, 0.027512957314741, -0.060711514345127, -0.925728053394680,
                     -0.033117806958892, -0.380932346698436, -0.493071060049599, 0.328947236201250],
                     [-0.173318365340601, -0.077142569984113, -0.076310560438528, 0.089225994308972,
                     -0.002018880643991, -0.013163178758400, -0.064330936170142, -0.237122798059981,
                     -0.172523534080677, -0.064784833527950, 0.277209132093551, 0.334394267761285,
                     -0.120700446096625, 0.092203859487363, 0.485868651300583, -0.262678175365571],
                     [0.051245491573803, 0.047735585799314, 0.019889459157673, -0.055381465402088,
                     0.171358401700705, 0.049314885561074, -0.021834316287122, 0.033901985721205,
                     0.121062561797019, 0.095099592970483, 0.052994957136868, -0.246427467822199,
                     -0.029773768604971, -0.153276054566094, -0.084954401825375, 0.052024137604019],
                     [-0.153612088210597, -0.058212221217499, 0.096467473348521, 0.129389831134947,
                     -0.166369564539914, -0.163654572235491, -0.019395649590667, -0.140682220131554,
                     -0.071530405390647, -0.016836777394500, -0.063409311447827, 0.372333201408698,
                     -0.061510980613705, 0.172726336659603, 0.223722169457708, -0.120402896064248]]
                     
args = sys.argv
args = args[1:] # First element of args is the file name

if len(args) < 3:
    print('Some Arguments are missing. Please enter all the arguments!')
else:
    for i in range(len(args)):
        inputStr = args[0]
        alg = args[1]
        encDec = args[2]
                     
#Function that converts a binary string into its ascii character
def toAscii(binaryString):
    return "".join([chr(int(binaryString[i:i+16],2)) for i in range(0,len(binaryString),16)])

#Function for Shift Algorithm Encryption     
def shiftEncryption(string, shift):
    encrypted = ''
    for c in string:
        if c == ' ':
            encrypted = encrypted + c 
        else:
            encrypted = encrypted + chr(ord(c) + shift)
    return encrypted

#Function for Shift Algorithm Deccryption     
def shiftDecryption(string, shift):
    decrypted = ''
    for c in string:
        if c == ' ':
            decrypted = decrypted + c 
        else:
            decrypted = decrypted + chr(ord(c) - shift)
    return decrypted

#Function for "Unit Testing" of the Shift Algorithm Encryption
def shiftTests():
    #Test small letter
    assert shiftEncryption('a',3) == 'd'
    #Test last small letter
    assert shiftEncryption('z',3) == '}'
    #Test capital letter
    assert shiftEncryption('B',3) == 'E'
    #Test last capital letter
    assert shiftEncryption('Z',3) == ']'
    #Test a word
    assert shiftEncryption('Hello',3) == 'Khoor'
    #Test a word with a space
    assert shiftEncryption('Hello World',3) == 'Khoor Zruog'
    #Test a word with two spaces
    assert shiftEncryption('Hello Big World',3) == 'Khoor Elj Zruog'
    #Test a word with special leter
    assert shiftEncryption('Hello World!!',3) == 'Khoor Zruog$$'
    print('All Shift Test Cases Succeeded')
                    

#Function for the Matrix Algorithm Encryption
def matrixEncryption(string, encryptMatrix):    
        
    charOrder = 0
    encryptedStr = ''
    w, h = 16, len(string);
    encoded = [[0 for x in range(w)] for y in range(h)]
    #changing each character in the string into its 16 binary bits
    for c in string:
        s = format(ord(c), '016b')
        #Putting the 16 binary bits in a 1d matrix form
        slist = list(s)
        #Adding the new character's matrix in a 2d matrix that carries all the binary numbers of the string
        for i in range(0, len(slist)):
            slist[i] = int(slist[i])
            encoded[charOrder][i] = slist[i]
        charOrder += 1
        
    #Multiplying the string matrix with the given encryption matrix
    w, h = 16, len(string);
    charEncode = [[0 for x in range(w)] for y in range(h)]
    for i in range(len(encoded)):
       for j in range(len(encryptMatrix[0])):
           for k in range(len(encryptMatrix)):
               charEncode[i][j] += encoded[i][k] * encryptMatrix[k][j]
           #Putting the encrypted matrix in a string form
           encryptedStr += str(int(charEncode[i][j])) + ','
    encryptedStr = encryptedStr[:-1]
    return encryptedStr

#Function for the Matrix Algorithm Decryption               
def matrixDecryption(encryptedStr, decryptMatrix):
    encryptedStr = encryptedStr.split(',')
    numOfLetters = len(encryptedStr) / len(decryptMatrix)
    numOfLetters = int(numOfLetters)
    w, h = 16, numOfLetters;
    encryptedMatrix = [[0 for x in range(w)] for y in range(h)]
    k = 0
    for i in range(numOfLetters):
        for j in range(len(decryptMatrix)):
            encryptedMatrix[i][j] = int(encryptedStr[k])
            k += 1
    w, h = len(encryptedMatrix[0]), len(encryptedMatrix);
    charDecode = [[0 for x in range(w)] for y in range(h)]
    for i in range(len(encryptedMatrix)):
       # iterate through columns of Y
       for j in range(len(decryptMatrix[0])):
           # iterate through rows of Y
           for k in range(len(decryptMatrix)):
               charDecode[i][j] += encryptedMatrix[i][k] * decryptMatrix[k][j]
           charDecode[i][j] = int(abs(round(charDecode[i][j])))
           
    binaryNum = ''
    for i in range(len(charDecode)):
        for j in range(len(charDecode[0])):
            binaryNum = binaryNum + str(charDecode[i][j])
        asciiNum = toAscii(binaryNum)
    return asciiNum


    
#Function for "Unit Testing" of the Matrix Algorithm Encryption
def matrixTests():
    #Test small letter
    assert matrixEncryption('a', encryptMatrix) == '14,17,4,15,8,11,7,1,3,14,15,13,12,20,19,3'
    #Test last small letter
    assert matrixEncryption('z', encryptMatrix) == '28,21,23,19,25,24,21,11,17,25,19,17,17,33,26,7'
    #Test capital letter
    assert matrixEncryption('B', encryptMatrix) == '7,4,7,6,13,5,11,3,3,6,9,5,7,9,11,5'
    #Test last capital letter
    assert matrixEncryption('Z', encryptMatrix) == '22,15,23,12,23,17,20,11,15,19,15,15,15,24,22,7'
    #Test a word
    assert matrixEncryption('Hello', encryptMatrix) == '9,6,12,7,13,8,13,9,4,8,8,6,6,11,13,5,18,19,5,22,8,13,14,10,5,22,20,20,18,27,20,12,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,28,25,18,27,26,24,28,20,10,36,25,21,29,43,28,16'
    #Test a word with a space
    assert matrixEncryption('Hello World', encryptMatrix) == '9,6,12,7,13,8,13,9,4,8,8,6,6,11,13,5,18,19,5,22,8,13,14,10,5,22,20,20,18,27,20,12,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,28,25,18,27,26,24,28,20,10,36,25,21,29,43,28,16,6,6,0,7,2,7,1,0,2,6,4,2,2,9,4,0,25,22,16,20,16,15,20,12,14,27,23,27,25,31,25,14,28,25,18,27,26,24,28,20,10,36,25,21,29,43,28,16,22,17,14,16,16,17,13,3,14,17,17,16,12,24,21,5,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,13,10,4,18,6,10,13,10,5,14,15,14,9,18,13,12'
    #Test a word with two spaces
    assert matrixEncryption('Hello Big World', encryptMatrix) == '9,6,12,7,13,8,13,9,4,8,8,6,6,11,13,5,18,19,5,22,8,13,14,10,5,22,20,20,18,27,20,12,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,28,25,18,27,26,24,28,20,10,36,25,21,29,43,28,16,6,6,0,7,2,7,1,0,2,6,4,2,2,9,4,0,7,4,7,6,13,5,11,3,3,6,9,5,7,9,11,5,20,21,13,18,17,18,15,9,6,22,17,14,17,29,24,5,22,21,9,24,17,17,20,12,7,28,23,20,24,34,23,14,6,6,0,7,2,7,1,0,2,6,4,2,2,9,4,0,25,22,16,20,16,15,20,12,14,27,23,27,25,31,25,14,28,25,18,27,26,24,28,20,10,36,25,21,29,43,28,16,22,17,14,16,16,17,13,3,14,17,17,16,12,24,21,5,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,13,10,4,18,6,10,13,10,5,14,15,14,9,18,13,12'
    #Test a word with special leter
    assert matrixEncryption('Hello!!', encryptMatrix) == '9,6,12,7,13,8,13,9,4,8,8,6,6,11,13,5,18,19,5,22,8,13,14,10,5,22,20,20,18,27,20,12,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,19,14,13,21,15,17,21,18,8,22,17,15,14,27,18,14,28,25,18,27,26,24,28,20,10,36,25,21,29,43,28,16,11,15,1,11,4,10,2,0,2,14,9,8,11,18,11,0,11,15,1,11,4,10,2,0,2,14,9,8,11,18,11,0'
    print('All Matrix Test Cases Succeeded')
    
#Function for the Reverse Algorithm Encryption 
def reverseEncryption(inputStr):
    url = 'http://backendtask.robustastudio.com/encode'
    payload = "{\n\t\"string\" : \"" + inputStr + "\"\n}"
    headers = {
      'Content-Type': 'application/json'
    }
    response = requests.request('POST', url, headers = headers, data = payload)
    return response.text

#Function for the Reverse Algorithm Decryption     
def reverseDecryption(reversedStr):    
    url = 'http://backendtask.robustastudio.com/decode'
    payload = "{\n\t\"string\" : \"" + inputStr + "\"\n}"
    headers = {
      'Content-Type': 'application/json'
    }
    response = requests.request('POST', url, headers = headers, data = payload)
    return response.text
       
#Function calls  
shiftTests()
matrixTests() 
result = ''            
if alg == "shift":
    if encDec == "encrypt":
        result = shiftEncryption(inputStr, 3)
    elif encDec == "decrypt":
        result = shiftDecryption(inputStr, 3)
    else: print('Third argument entered is not correct')

elif alg == "reverse":
    if encDec == "encrypt":
        result = reverseEncryption(inputStr)
    elif encDec == "decrypt":
        result = reverseDecryption(inputStr)
    else: print('Third argument entered is not correct')

elif alg == "matrix":
    if encDec == "encrypt":
        result = matrixEncryption(inputStr, encryptMatrix)
    elif encDec == "decrypt":
        result = matrixDecryption(inputStr, decryptMatrix)
    else: print('Third argument entered is not correct')
else: print('Second argument entered is not correct')
print(result)