# BackendTask

# Project Decription

This project is made to implement shift, matrix and reverse encryption and decryption algorithms for strings and implement unit testing for them.

# Prerequisites

You must install python, pip and requests

# Running The Project

The project contains only 1 python file. All you need to do is to run this file from the command window and pass the arguments after installing the required libraries mentioned in the prerequisites.
You'll need to mention the file name, then the string need to be encrypted/decrypted, followed by the algorithm name, and finally choose wether encrypt ord decrypt.

Example:
Encrypting "Hello" using Shift Algorithm

`python mainFile.py "Hello" shift encrypt`




